\select@language {czech}
\contentsline {section}{\numberline {1}Co je to Git?}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Git Bash}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Bitbucket}{2}{subsection.1.2}
\contentsline {section}{\numberline {2}Zakl\IeC {\'a}d\IeC {\'a}me projekt ve slu\IeC {\v z}b\IeC {\v e} Git}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Vytvo\IeC {\v r}en\IeC {\'\i } projektu na Bitbucket}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Stahujeme projekt z Bitbucket}{8}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Sta\IeC {\v z}en\IeC {\'\i } ciz\IeC {\'\i }ho projektu}{9}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Sta\IeC {\v z}en\IeC {\'\i } vlastn\IeC {\'\i }ho projektu}{10}{subsubsection.2.2.2}
\contentsline {section}{\numberline {3}Pracujeme s projektem}{12}{section.3}
\contentsline {subsection}{\numberline {3.1}Stahujeme aktu\IeC {\'a}ln\IeC {\'\i } verzi projektu}{12}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Nahr\IeC {\'a}v\IeC {\'a}me zm\IeC {\v e}ny na internet}{13}{subsection.3.2}
